import { Component, Input } from "@angular/core";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';

import { DEMO } from "../demo-summaries";

@Component({
    selector: 'result',
    templateUrl: '../templates/result.component.html',
    styleUrls: ['../styles/result.component.css']
})
export class ResultComponent {
    demo = DEMO;

    search: String;
    private route$ : Subscription;
    constructor(private route: ActivatedRoute, private router: Router) {}

    ngOnInit() {
        this.route$ = this.route.params.subscribe(
            (params : Params)=> {
                this.search = params["search"];
            }
        );
    }
    ngOnDestroy() {
        if(this.route$) this.route$.unsubscribe();
    }
}

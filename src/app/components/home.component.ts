import { Component } from "@angular/core";

@Component({
    selector: 'home',
    templateUrl: '../templates/home.component.html',
    styleUrls: ['../styles/app.component.css']
})
export class HomeComponent {
    title = "TellMe"
}

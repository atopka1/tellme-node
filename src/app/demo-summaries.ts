export const DEMO = { "results" : [
    {
        "keyword" : "trump",
        "image" : "",
        "summary" : "",
        "bias" : [
            {
                "description" : "",
                "sources" : [
                    "cnn",
                    "bnc"
                ]
            },
            {
                "description" : "",
                "sources" : [
                    "fox",
                    "aljazeera"
                ]
            }
        ],
        "bibliography" : ""
    },
    {
        "keyword" : "syria",
        "image" : "",
        "summary" : "",
        "bias" : [
            {
                "description" : "",
                "sources" : [
                    "cnn",
                    "bnc"
                ]
            },
            {
                "description" : "",
                "sources" : [
                    "fox",
                    "aljazeera"
                ]
            }
        ],
        "bibliography" : ""
    },
    {
        "keyword" : "",
        "image" : "",
        "summary" : "",
        "bias" : [
            {
                "description" : "",
                "sources" : [
                    "cnn",
                    "bnc"
                ]
            },
            {
                "description" : "",
                "sources" : [
                    "fox",
                    "aljazeera"
                ]
            }
        ],
        "bibliography" : ""
    },
    {
        "keyword" : "",
        "image" : "",
        "summary" : "",
        "bias" : [
            {
                "description" : "",
                "sources" : [
                    "cnn",
                    "bnc"
                ]
            },
            {
                "description" : "",
                "sources" : [
                    "fox",
                    "aljazeera"
                ]
            }
        ],
        "bibliography" : ""
    },
    {
        "keyword" : "",
        "image" : "",
        "summary" : "",
        "bias" : [
            {
                "description" : "",
                "sources" : [
                    "cnn",
                    "bnc"
                ]
            },
            {
                "description" : "",
                "sources" : [
                    "fox",
                    "aljazeera"
                ]
            }
        ],
        "bibliography" : ""
    },
]}

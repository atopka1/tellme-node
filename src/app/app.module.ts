import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { RouterModule, Routes } from "@angular/router";

import { AppComponent }         from './app.component';
import { AppRoutingModule }     from "./app-routing.module";

import { HeaderComponent }      from "./components/header.component";
import { ResultComponent }      from "./components/result.component";
import { AboutComponent }       from "./components/about.component";
import { HomeComponent }        from "./components/home.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ResultComponent,
    AboutComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
